# osasun

This project will one day become a weight/calorie tracker + food search engine.
It's killer feature is _natural language input_, for which it currently uses the ![Nutritionix API](https://developer.nutritionix.com/docs/v2).
In the future, the NLP stuff should be handled via PyTorch or the like.

## why "osasun"?

It means _health_ in Basque. :)

## how to demo

1. Set the `NIX_API_KEY` and `NIX_APP_ID` environment variables
2. Find a recipe you like from one of the sites supported by [`recipe-scrapers`](https://github.com/hhursev/recipe-scrapers#scrapers-available-for)
3. Run the following example:

```
export URL_TO_SCRAPE="<insert your URL here>"
python scrape_recipe --url $URL_TO_SCRAPE | python estimate_macros.py | jq
```

Note: requires `jq` to work.
