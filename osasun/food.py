#!/usr/bin/env python

# food.py
# Contains classes for management of food-related entities

# Notes
# - The "canonical" fallback unit is the gram.
# - The "canonical" fallback serving is 100 g

from typing import Dict, Union, List, Optional
import attr

JSONType = Dict[Union[str, float, int], Union[str, float, int]]


@attr.s(frozen=True)
class Amount:
    unit: str = attr.ib(validator=attr.validators.instance_of(str))
    quantity: float = attr.ib(
        validator=attr.validators.instance_of(float), converter=float
    )

    rounding: int = attr.ib(default=3)

    def as_grams(self):
        # TODO: incorporate unit conversion library
        raise NotImplementedError

    def __add__(self, other):
        if self.unit != other.unit:
            return self.as_grams() + other.as_grams()
        else:
            return Amount(unit=self.unit, quantity=self.quantity + other.quantity)

    def __truediv__(self, d: int):
        return Amount(unit=self.unit, quantity=self.quantity / d)

    def to_dict(self):
        return {
            "unit": self.unit,
            "quantity": round(self.quantity, self.rounding)
            if self.rounding
            else self.quantity,
        }


@attr.s(frozen=True)
class Gram(Amount):

    unit: str = attr.ib(default="g")


@attr.s(frozen=True)
class Macros:

    total_calories: Amount = attr.ib()
    protein: Amount = attr.ib()
    carbohydrates: Amount = attr.ib()
    fat: Amount = attr.ib()

    def __add__(self, other):
        return Macros(
            total_calories=self.total_calories + other.total_calories,
            protein=self.protein + other.protein,
            carbohydrates=self.carbohydrates + other.carbohydrates,
            fat=self.fat + other.fat,
        )

    def __truediv__(self, d: int):
        return Macros(
            total_calories=self.total_calories / d,
            protein=self.protein / d,
            carbohydrates=self.carbohydrates / d,
            fat=self.fat / d,
        )

    def to_dict(self) -> JSONType:

        return {
            "protein": self.protein.to_dict(),
            "carbohydrates": self.carbohydrates.to_dict(),
            "fat": self.fat.to_dict(),
            "total_calories": self.total_calories.to_dict(),
        }


@attr.s(frozen=True)
class FoodItem:
    name: str = attr.ib()
    serving_size: Amount = attr.ib()
    macros: Macros = attr.ib()

    def to_json(self) -> JSONType:
        raise NotImplementedError

    def macros_in(self, amount: Amount):
        """Returns a new FoodItem with macros corresponding to amount"""
        raise NotImplementedError

    def to_dict(self):

        return {
            "name": self.name,
            "serving_size": self.serving_size.to_dict(),
            "macros": self.macros.to_dict(),
        }

    @classmethod
    def from_dict(
        cls,
        json_obj: JSONType,
        serving_size_key: str = "serving_size",
        total_calories_key: str = "total_calories",
        protein_key: str = "protein",
        carbohydrates_key: str = "carbohydrates",
        fat_key: str = "fat",
        name_key: Optional[str] = "name",
        name: Optional[str] = None,
    ) -> "FoodItem":

        assert (
            name is not None or name_key is not None
        ), "Must supply either name or name key!"

        if not name:
            name = json_obj[name_key]

        # handle serving size
        d_serving_size: JSONType = json_obj[serving_size_key]
        serving_quantity = d_serving_size["quantity"]
        serving_unit = d_serving_size["unit"]
        serving_amount = Amount(unit=serving_unit, quantity=serving_quantity)

        # handle total calories
        total_calories_amount = Amount(
            quantity=json_obj[total_calories_key], unit="calorie"
        )

        # get amount of protein
        protein_amount = Amount(quantity=json_obj[protein_key], unit="gram")
        carbohydrates_amount = Amount(quantity=json_obj[carbohydrates_key], unit="gram")
        fat_amount = Amount(quantity=json_obj[fat_key], unit="gram")

        # build macros
        serving_macros = Macros(
            total_calories=total_calories_amount,
            protein=protein_amount,
            carbohydrates=carbohydrates_amount,
            fat=fat_amount,
        )

        return cls(serving_size=serving_amount, macros=serving_macros, name=name)


@attr.s(frozen=True)
class Recipe(FoodItem):
    ingredients: List[FoodItem] = attr.ib()
    steps: List[str] = attr.ib()

    @property
    def total_macros(self) -> Union[int, Macros]:
        return sum(ing.macros for ing in self.ingredients)
