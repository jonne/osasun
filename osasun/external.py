#!/usr/bin/env python

# external.py
# Contains classes used for external data fetching.

from typing import Dict, Union, List
import requests

JSONType = Dict[Union[str, float, int], Union[str, float, int]]


class NutritionFinder:

    API_URL = "https://trackapi.nutritionix.com/v2/natural/nutrients"

    def __init__(
        self,
        app_id: str,
        api_key: str,
        timezone="US/Eastern",
        keep_fields_as: JSONType = {},
    ) -> None:
        self.app_id = app_id
        self.api_key = api_key
        self.timezone = timezone

        if keep_fields_as:
            self.keep_fields_as = keep_fields_as
        else:
            self.keep_fields_as = {
                "food_name": "name",
                "serving_qty": "quantity",
                "serving_unit": "unit",
                "serving_weight_grams": "grams",
                "nf_calories": "calories",
                "nf_total_fat": "total_fat",
                "nf_saturated_fat": "saturated_fat",
                "nf_cholesterol": "cholesterol",
                "nf_sodium": "sodium",
                "nf_total_carbohydrate": "carbohydrates",
                "nf_dietary_fiber": "fiber",
                "nf_sugars": "sugars",
                "nf_protein": "protein",
                "nf_potassium": "potassium",
            }

    def query(
        self, query: str, return_full: bool = False, prettify: bool = False
    ) -> List[JSONType]:

        header: JSONType = {"x-app-id": self.app_id, "x-app-key": self.api_key}

        data: JSONType = {"query": query, "timezone": self.timezone}

        responses: List[JSONType] = requests.post(
            self.API_URL, headers=header, json=data
        ).json()["foods"]

        responses = self.simplify(responses) if not return_full else responses
        responses = [self.prettify(r) if prettify else r for r in responses]

        return responses

    def simplify(self, responses: List[JSONType]) -> List[JSONType]:

        filtered = [
            {
                self.keep_fields_as[field]: value
                for field, value in response.items()
                if field in self.keep_fields_as
            }
            for response in responses
        ]

        return filtered

    def prettify(self, json_obj: JSONType) -> JSONType:

        prettify_keys = {
            "name": "Name",
            "quantity": "Quantity",
            "unit": "Unit",
            "grams": "Grams",
            "calories": "Calories",
            "total_fat": "Total Fat",
            "saturated_fat": "Saturated Fat",
            "cholesterol": "Cholesterol",
            "sodium": "Sodium",
            "carbohydrates": "Carbohydrates",
            "fiber": "Fiber",
            "sugars": "Sugars",
            "protein": "Protein",
            "potassium": "Potassium",
        }

        return {new: json_obj[old] for old, new in prettify_keys.items()}
